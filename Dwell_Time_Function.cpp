/*
 * Dwell_Time_Function.cpp
 * Created: 28-6-2018 08:18:05
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Timer Variables.
unsigned long Timer_7;
//Set timer to true when timer is finished.
bool TimedOut_7 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_7;

//Setup Code.
void Setup_Dwell_Time_Function(void)
{
	//Timer initialization.
	TimedOut_7 = false;
	//Start Timer.
	Timer_7 = millis();
}

//Function Code.
void Dwell_Time_Function(void)
{	
	if ((!TimedOut_7) && ((millis() - Timer_7) > INTERVAL_7))
	{
		//Timed out to deactivate timer.
		TimedOut_7 = true;

		//Check if Dwell is activated.
		if (Dwell_Time_Active == true)
		{
			//De-active Dwell Timer.
			Dwell_Time_Active = false;
		}
	}
	//Set Dwell Time.
	if (Dwell_Time_HMI == 0)
	{
	INTERVAL_7 = 200;
	}
	else if (Dwell_Time_HMI > 0)	
	{
	INTERVAL_7 = Dwell_Time_HMI;
	}
	//Timer Start. Activate Dwell.
	if (Press_Complete == true && INTERVAL_7 > 0)
	{
		TimedOut_7 = false;
		Timer_7 = millis();
		Dwell_Time_Active = true;
	}
}