/*
 * Initialization_Function.cpp
 * Created: 29-6-2018 12:25:07
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool MCU1_Start_Bit;

//Function Code.
void Initialization_Function(void)
{
	//Activate initialization.
	if (!MCU1_Start_Bit)
	{
		Timer1_Start=true;
	}
	//Deactivate Initialization.
	else if (Timer1_Start == true)
	{
		MCU1_Start_Bit=true;
		Timer1_Start=false;
	}
}