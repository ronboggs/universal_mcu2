/*
 * Shuttle_Function.cpp
 * Created: 16-7-2018 17:08:15
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
int Shuttle_Retraction_Zone;
int Shuttle_Extend_Time = 500;
int Shuttle_Retract_Time = 500;
bool Up_Movement_Detection;
bool Read_Shuttle_Coil_Active;

//CET set value.
int CetValue_Shuttle_Function;

//Timer Variables.
unsigned long Timer_5;
//Set timer to true when timer is finished.
bool TimedOut_5 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_5;
//Timer Variables.
unsigned long Timer_6;
//Set timer to true when timer is finished.
bool TimedOut_6 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_6;

//HMI Modbus Variables.

//Setup Code.
void Setup_Shuttle_Function(void)
{
	//Timer initialization.
	TimedOut_5 = false;
	//Start Timer.
	Timer_5 = millis();
	//Timer initialization.
	TimedOut_6 = false;
	//Start Timer.
	Timer_6 = millis();
}

//Function Code.
void Shuttle_Function(void)
{
//Tooling Mode 0 = Manual.
//Tooling Mode 1 = ABFT.
//Tooling Mode 2 = Shuttle.			
	
	//Read analog CET value.
	CetValue_Shuttle_Function = Analog_CET;
		
	//Fastener Detection Shuttle Position.
	Shuttle_Retraction_Zone = Fastener_Detection_Shuttle_Calibration_Pos_HMI - Fastener_Detection_Shuttle_Offset_Pos_HMI;
		
	//Shuttle Retract function.
	if (((Tooling_Mode_HMI == 0 || Tooling_Mode_HMI == 1) || (CetValue_Shuttle_Function < Shuttle_Retraction_Zone) || digitalRead(MAS1_EJECT) == HIGH || Hyd_Cyl_Up_Release_Active == true || Up_Movement_Detection == true || Dwell_Time_Active == true) || (Robot_Shuttle_Lock == true && Machine_State_Robot_Mode_Active == true))
	{
		digitalWrite(SHUTTLE_EXTEND, LOW);
	}
	else
	{
		digitalWrite(SHUTTLE_EXTEND, HIGH);
	}
	
	//Shuttle Extend error handling.
	if ((!TimedOut_5) && ((millis() - Timer_5) > INTERVAL_5))
	{
		//Timed out to deactivate timer.
		TimedOut_5 = true;

		//Check if Shuttle Extend sensor is activated after 500ms.
		if (digitalRead(SHUTTLE_EXTENDED) == LOW)
		{
			//De-active Timer.
			Shuttle_Extend_Error = true;
		}
	}
	//Set Shuttle Extend Timer.
	INTERVAL_5 = Shuttle_Extend_Time;
	
	//Timer Start.
	if ((digitalRead(SHUTTLE_EXTENDED) == LOW && digitalRead(SHUTTLE_RETRACTED) == HIGH && (INTERVAL_5 > 0)) || digitalRead(SHUTTLE_EXTENDED) == HIGH)
	{
		TimedOut_5 = false;
		Timer_5 = millis();
		Shuttle_Extend_Error = false;
	}
	//Shuttle Retract Error handling.
	if ((!TimedOut_6) && ((millis() - Timer_6) > INTERVAL_6))
	{
		//Timed out to deactivate timer.
		TimedOut_6 = true;

		//Check if Shuttle sensor are still OK.
		if (digitalRead(SHUTTLE_RETRACTED) == LOW)
		{
			//De-active Timer.
			Shuttle_Retract_Error = true;
		}
	}
	//Set Shuttle Retract Timer.
	INTERVAL_6 = Shuttle_Retract_Time;
		
	//Timer Start.
	if ((digitalRead(SHUTTLE_RETRACTED) == LOW && digitalRead(SHUTTLE_EXTENDED) == HIGH && (INTERVAL_6 > 0)))
	{
		TimedOut_6 = false;
		Timer_6 = millis();
		Shuttle_Retract_Error = false;
	}
	// Up Movement Detection to prevent the shuttle will move to front position above shuttle reference when using up pedal.
	if (digitalRead(FOOT_UP) == true)
	{
		Up_Movement_Detection = true;
	}
	else if (CetValue_Shuttle_Function >= TOS)
	{
		Up_Movement_Detection = false;
	}
	//Read coils. Reset TPS error. MCU is master at reset. HMI will get only ACK.
	if (!Safety_Sensor_Error == true && !SSSF_Safety_Sensor_Error == true && !TPS_Error == true && !Fastener_Detection_Error == true && !Fastener_Length_Error == true && !FD_Retry_Set == true && !Shuttle_Extend_Error == true && !Shuttle_Retract_Error == true)
	{
		Error_Number_HMI = 0;
		Error_Set_HMI = 0;
		Read_Shuttle_Coil_Active = false;
	}
	//Write coils. Set HMI into error mode. Display TPS error by error number 35.
	else if (Shuttle_Retract_Error == true && !Read_Shuttle_Coil_Active == true)
	{
		Read_Coil_48 = false;
		Write_Coil_48 = true;
		Read_Coil_73 = false;
		Write_Coil_73 = true;
		Error_Set_HMI = 1;
		Error_Number_HMI = 35;
		Read_Shuttle_Coil_Active = true;
	}
	//Write coils. Set HMI into error mode. Display TPS error by error number 34.
	else if (Shuttle_Extend_Error == true && !Read_Shuttle_Coil_Active == true)
	{
		Read_Coil_48 = false;
		Write_Coil_48 = true;
		Read_Coil_73 = false;
		Write_Coil_73 = true;
		Error_Set_HMI = 1;
		Error_Number_HMI = 34;
		Read_Shuttle_Coil_Active = true;
	}
	//Ensure that Shuttle is Extended before Cylinder Down movement.
	//if ((digitalRead(SHUTTLE_EXTENDED) == HIGH && !Shuttle_Extend_Check_Global == true) || (Tooling_Mode_HMI == 0 || Tooling_Mode_HMI == 1))
	if (digitalRead(SHUTTLE_EXTENDED) == HIGH || (Tooling_Mode_HMI == 0 || Tooling_Mode_HMI == 1))
	{
		Shuttle_Extend_Check_Global = true;
	}
	//if ((Tooling_Mode_HMI == 2) && (digitalRead(SHUTTLE_EXTENDED) == LOW && digitalRead(DOWN_SOLENOID) == LOW))
	if ((Tooling_Mode_HMI == 2) && digitalRead(SHUTTLE_EXTENDED) == LOW && (CetValue_Shuttle_Function >= TOS))
	{
		Shuttle_Extend_Check_Global = false;
	}
}
