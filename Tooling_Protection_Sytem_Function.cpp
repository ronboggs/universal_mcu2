/*
 * TPS_Function.cpp
 * Created: 3-7-2018 19:02:58
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Read_TPS_Coil_Active;

//Timer Variables.
unsigned long Timer_13;
//Set timer to true when timer is finished.
bool TimedOut_13 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_13;
//Fastener Detection Delay Var.
bool TPS_Delay;
//Fastener Detection Delay Time.
int TPS_Delay_Time = 800;

//CET set value.
int CetValue_TPS_Function;

//Setup Code.
void Setup_Tooling_Protection_System_Function(void)
{
	//Timer initialization.
	TimedOut_13 = false;
	//Start Timer.
	Timer_13 = millis();
}

//Function Code.
void Tooling_Protection_System_Function(void)
{
	//Read CET Analog input.
	CetValue_TPS_Function = Analog_CET;
	
	//TPS Measurement Active to ensure no error message will displayed during setup.
	if (!Setup_Stroke_HMI == true && !TPS_Set_HMI == true)
	{
		//TPS function is always active. Not possible to deactivate. TPS Check above Ref.
		if (TPS_Error == true || (Safety_Sensor_Switched_State == true && (CetValue_TPS_Function > (TPS_Calibrated_Upper_Value_HMI + TPS_Offset_Value_HMI))) || TPS_Delay == true)
		{
			TPS_Error = true;
		}
		//TPS Check below Ref.
		else if (TPS_Error == true || (Safety_Sensor_Switched_State == false && (CetValue_TPS_Function < (TPS_Calibrated_Upper_Value_HMI - TPS_Offset_Value_HMI)) && digitalRead(DOWN_SOLENOID) == HIGH) || TPS_Delay == true)
		{
			TPS_Error = true;
		}
	}
	//Read coils. Reset TPS error. MCU is master at reset. HMI will get only ACK.
	if (!Safety_Sensor_Error == true && !SSSF_Safety_Sensor_Error == true && !TPS_Error == true && !Fastener_Detection_Error == true && !Fastener_Length_Error == true && !FD_Retry_Set == true && !Shuttle_Extend_Error == true && !Shuttle_Retract_Error == true)
	{	
		Error_Number_HMI = 0;
		Error_Set_HMI = 0;
	}
	//Write coils. Set HMI into error mode. Display TPS error by error number 2. Ensure that no TPS error message is send to HMI during SS-Error.
	if (TPS_Error == true && (SSSF_Safety_Sensor_Error == false || Safety_Sensor_Error == false))
	{		
		Read_Coil_48 = false;
		Write_Coil_48 = true;
		Read_Coil_73 = false;
		Write_Coil_73 = true;
		Error_Set_HMI = 1;
		Error_Number_HMI = 2;
	}
	//TPS Timer. Delay Error Reset Signal (500ms).
	if ((!TimedOut_13) && ((millis() - Timer_13) > INTERVAL_13))
	{
		//Timed out to deactivate timer.
		TimedOut_13 = true;

		//Check if TPS Delay is activated.
		if (TPS_Delay == true)
		{
			//De-active Timer.
			TPS_Delay = false;
		}
	}
	//Set TPS Delay Time.
	{
		INTERVAL_13 = TPS_Delay_Time;
	}
	//Timer Start. Activate TPS Delay Timer.
	if (TPS_Error == true && (CetValue_TPS_Function < TOS) && (INTERVAL_13 > 0))
	{
		TimedOut_13 = false;
		Timer_13 = millis();
		TPS_Delay = true;
	}
}