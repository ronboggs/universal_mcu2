/*
 * Safety_Block_Function.cpp
 * Created: 28-6-2018 08:23:30
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Safety_Block_Function(void)
{
	if (digitalRead(ESTOP2) == HIGH)
	{
		EMG_System_Active_HMI = true;
		EMG_System_Active_MCU2 = true;
	} 
	else
	{
		EMG_System_Active_HMI = false;
		EMG_System_Active_MCU2 = false;
	}
}