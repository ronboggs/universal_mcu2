/*
 * Feed_Fastener_Function.cpp
 * Created: 27-6-2018 17:32:10
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Timer Variables.
unsigned long Timer_3;
//Set timer to true when timer is finished.
bool TimedOut_3 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_3;

//HMI Modbus Variables.

//Setup Code.
void Setup_Feed_Fastener_Function(void)
{
	//Timer initialization.
	TimedOut_3 = false;
	//Start Timer.
	Timer_3 = millis();
}

//Function Code.
void Feed_Fastener_Function(void)
{
	if ((!TimedOut_3) && ((millis() - Timer_3) > INTERVAL_3))
	{
		//Timed out to deactivate timer.
		TimedOut_3 = true;

		//Check if MAS Eject is activated.
	if (digitalRead(MAS1_EJECT) == HIGH)
	{
		//De-active MAS Eject Timer.
		MAS_Feed_Fastener_Active = false;
	}
	}
	//Set MAS1 Eject Time.
	INTERVAL_3 = MAS_Eject_Time_HMI;
		
	//Timer Start. Activate MAS HMI Eject, Press_Complete or Robot Eject.
	//Tooling Mode 0 = Manual.
	//Tooling Mode 1 = ABFT.
	//Tooling Mode 2 = Shuttle.
	if ((Tooling_Mode_HMI == 1 || Tooling_Mode_HMI == 2) && (MAS_Feed_HMI == 1 || (Robot_Feed_Fastener_MCU2 == true && Machine_State_Robot_Mode_Active == true) || Press_Complete == true || Fastener_Detection_Error == true) && (INTERVAL_3 > 0))
	{
		TimedOut_3 = false;
		Timer_3 = millis();
		MAS_Feed_Fastener_Active = true;
		digitalWrite(MAS1_EJECT, HIGH);
	}
}
