/*
 * Timer_Function.cpp
 * Created: 29-6-2018 12:30:34
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Timer1_Set;

//each "event" (LED) gets their own tracking variable
unsigned long previousMillisTimer1=0;

//Different intervals for each Timer.
int intervalTimer1 = 2000;

//The Timer1State1
bool Timer1state = false;

//Function Code.
void Timer_Function(void)
{
	//Get current time stamp.
	//Only need one for more then one if-statements.
	
	if (Timer1_Start == true && !Timer1_Q == true)
	{
		unsigned long currentMillis = millis();
		//Time to active Timer state to set Active. TOF (OFF Timer) use "false" statement (Timer_Q). TON (ON Timer) use "true" statement(Timer_Q).
		if ((unsigned long)(currentMillis - previousMillisTimer1) >= intervalTimer1)
		{
			Timer1state = !Timer1state;
			Timer1_Set = Timer1state;
			//Save current time to Pin or variable previousMillis.
			previousMillisTimer1 = currentMillis;
		}
		
		if (Timer1_Set == true)
		{
			Timer1_Q=true;
		}
	}
}