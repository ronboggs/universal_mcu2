/*
 * Up_Travel_Function.cpp
 * Created: 13-8-2018 09:11:57
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
int Up_Travel_Local;
int Press_Complete_Position_Local;

//CET set value.
int CetValue_Up_Travel_Function;

//Function Code.
void Up_Travel_Function(void)
{
	//Read analog CET value.
	CetValue_Up_Travel_Function = Analog_CET;
	
	if (Press_Complete == true)
	{
		Press_Complete_Position_Local = CetValue_Up_Travel_Function;	
	}
	//Up travel is based on Press Complete signal.
	if (Tooling_Mode_HMI == 0 || Tooling_Mode_HMI == 1)
	{
		Up_Travel_Local = ((((TOS_Position_HMI - Press_Complete_Position_Local)/100)*Up_Travel_HMI) + Press_Complete_Position_Local);
	}
	else
	{
		Up_Travel_Local = TOS_Position_HMI;
	}
	
	if (CetValue_Up_Travel_Function >= Up_Travel_Local)
	{
		Up_Travel_Cylinder_Stop = true;
	}
	else
	{
		Up_Travel_Cylinder_Stop = false;
	}
}
