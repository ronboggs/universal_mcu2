// Main_Siemens_Automation_824MSPe.h

#ifndef _MAIN_SIEMENS_AUTOMATION_824MSPE_h
#define _MAIN_SIEMENS_AUTOMATION_824MSPE_h

//Define Cpp Functions.

//Foot pedal Function.
void Feed_Fastener_Function();
//Press_Complete_Function.
void Footpedal_Function();
//Initialization Function.
void Initialization_Function();
//Machine State Function.
void Machine_State_Function();
//MAS Function.
void MAS_Function();
//Feed Fastener Function.
void Safety_Sensor_State_Function();
//Timer Function.
void Timer_Function();
//Tooling contact Function.
void Toolingcontact_Function();
//Modbus Communication.
void Io_Poll();
//Non Conductive Mode Function.
void Non_Conductive_Mode_Function();
//Conductive Mode Function.
void Conductive_Mode_Function();
//Alarm Function.
void Alarm_Function();
//TPS Function.
void Tooling_Protection_System_Function();
//Setup Stroke Function.
void Setup_Stroke_Vacuum_Detection_Function();
//Fastener Detection Function.
void Fastener_Detection_Function();
//Fastener Length Function.
void Fastener_Length_Function();
//Setup Feed Fastener Function.
void Setup_Feed_Fastener_Function();
//Setup Press_Complete Function.
void Setup_Press_Complete_Function();
//Press Complete Function.
void Press_Complete_Function();
//MAS Setup Blow Off Function.
void Setup_MAS_Blow_Off_Function();
//MAS Blow Off Function.
void MAS_Blow_Off_Function();
//Setup Shuttle Function.
void Setup_Shuttle_Function();
//Shuttle Function.
void Shuttle_Function();
// Hydraulic Cylinder Function.
void Hydraulic_Cylinder_Function();
//Dwell Time Function.
void Dwell_Time_Function();
//Setup Dwell Time Function.
void Setup_Dwell_Time_Function();
//Vacuum Generator Function.
void Vacuum_Generator_Function();
//Safety Block Function.
void Safety_Block_Function();
//Top Of Stroke Function.
void TOS_Function();
//Setup Stroke TPS Function.
void Setup_Stroke_TPS_Function();
//Setup Stroke Fastener Length Function.
void Setup_Stroke_Fastener_Length_Function();
//Modbus Communication Function.
void Modbus_Communication_Function();
// Setup Modbus Communication Function.
void Setup_Modbus_Communication_Function();
// Up Travel Function.
void Up_Travel_Function();
//Setup Hydraulic Cylinder Function.
void Setup_Hydraulic_Cylinder_Function();
//Robot_Communication_Function.
void Robot_Communication_Function();
//All at Position Function.
void All_At_Position_Function();
//Setup Fastener Detection Function.
void Setup_Fastener_Detection_Function();
//MCU version function.
void MCU_Version_Function();
//Setup Fastener Length Detection Function.
void Setup_Fastener_Length_Detection_Function();
//Setup Conductive Mode Function.
void Setup_Conductive_Mode_Function_Function();
//Setup TPS Function.
void Setup_Tooling_Protection_System_Function();
//
void RequestEvent_Send();
//
void RequestEvent_Receive(int);


//Global Variables. Start definition at Main_Siemens_Automation_824MSPe cpp file.
extern bool Safety_Sensor_Error;
extern bool Machine_Init_State;
extern bool Machine_Idle_State;
extern bool Machine_Run_State;
extern bool Toolingcontact_Switched_State;
extern bool Safety_Sensor_Switched_State;
extern bool Non_Conductive_Stop1;
extern bool Non_Conductive_Stop2;
extern bool Press_Complete;
extern bool TPS_Error;
extern bool Dwell_Time_Active;
extern bool Fastener_Detection_Error;
extern bool Fastener_Length_Error;
extern bool MAS_Feed_Fastener_Active;
extern bool MAS_Blow_Off_Active;
extern bool MAS_Vibration_Active;
extern bool Hyd_Cyl_Down_Release_Active;
extern bool Hyd_Cyl_Up_Release_Active;
extern bool Press_Complete_Active;
extern int TOS;
extern bool Vac_Without_Fastener_Down_Hold;
extern bool Vac_With_Fastener_Down_Hold;
extern bool Hyd_Cyl_Dow_Release_Active_1;
extern bool Vacuum_Detection_Measurement_Active;
extern int Fastener_Detection_Zone;
extern bool Up_Travel_Cylinder_Stop;
extern bool FLD_Measurment_Active;
extern bool FD_Retry_Set;
extern bool SSSF_Safety_Sensor_Error;
extern bool Error_Message;
extern bool Machine_State_Robot_Mode_Active;
extern bool EMG_Error_Reset_Local;
extern bool Robot_Error_Reset_Input;
extern bool Robot_Press_Signal_Active;
extern bool All_At_Position;
extern bool Lower_Tool_Pin_Up_Sensor;
extern bool Lower_Tool_Pin_Down_Sensor;
extern int Analog_CET;
extern int Analog_VAC;
extern int Analog_PRESSURESWITCH;
extern bool FL_Setup_Stroke_Reset;
extern bool Robot_Shuttle_Lock;
extern int MCU2_Version_Number;
extern bool Shuttle_Extend_Check_Global;
extern bool Setup_Stroke_TPS;

//Global Variables from or Set to MCU2.
extern bool Robot_Mode_Active_MCU2;
extern bool Robot_Error_Reset_MCU2;
extern bool EMG_Error_Reset_MCU2;
extern bool EMG_System_Active_MCU2;
extern bool All_At_Position_MCU2;
extern bool Robot_Press_Active_MCU2;
extern bool Lower_Tool_Pin_Down_Sensor_MCU2;
extern bool Lower_Tool_Pin_Up_Sensor_MCU2;
extern bool MCU_Lower_Tool_Pin_Up_Output;
extern bool MCU_Lower_Tool_Pin_Down_Output;
extern bool Robot_Shuttle_Lock_MCU2;
extern bool Dwell_Time_Active_MCU2;
extern bool Setup_Stroke_MCU2;
extern bool Shuttle_Retract_MCU2;
extern bool Shuttle_Extend_MCU2;
extern bool Robot_Feed_Fastener_MCU2;
extern bool Up_Signal_MCU2;
extern bool Down_Solenoid_Block_MCU2;

//Quality (QA) Variables.
extern int Vacuum_Switch_AI;

//Modbus HMI Variables.
extern bool Run_Setup_Mode_HMI;				//=1
extern bool ConductiveM_HMI;
extern bool Non_ConductiveM_HMI;			//=1
extern int Dwell_Time_HMI;
//Upper TPS Value.
extern int TPS_Calibrated_Upper_Value_HMI;
//Lower TPS Value.
extern int TPS_Calibrated_Lower_Value_HMI;
//TPS Offset value.
extern int TPS_Offset_Value_HMI;
//Setup Stroke HMI.
extern bool Setup_Stroke_HMI;
//Fastener Detection HMI.
extern bool Fastener_Detection_HMI;
// Fastener Detection Sensitivity.
extern int Fastener_Detection_Sensitivity_HMI;
//Fastener Length HMI.
extern bool Fastener_Length_HMI;
//Fastener Detection Error HMI.
extern bool Fastener_Detection_Error_HMI;
//Fastener Length Error HMI.
extern bool Fastener_Length_Error_HMI;
//Fastener Detection Calibrated Value HMI.
extern int Fastener_Detection_Calibrated_Value_HMI;
//Fastener Length Calibration Value HMI.
extern int Fastener_Length_Calibrated_Value_HMI;
//Fastener Detection Shuttle Calibration Position HMI.
extern int Fastener_Detection_Shuttle_Calibration_Pos_HMI;
//Fastener Detection Shuttle Offset_Position HMI.
extern int Fastener_Detection_Shuttle_Offset_Pos_HMI;
//Fastener Length Offset HMI.
extern int Fastener_Length_Sensitifity_HMI;
//Shuttle Extend Error.
extern bool Shuttle_Extend_Error;
//Shuttle Retract Error.
extern bool Shuttle_Retract_Error;
//Error Reset HMI.
extern bool Error_Set_HMI;
//EMG Error HMI.
extern bool EMG_Error_HMI;
//EMG Active.
extern bool EMG_System_Active_HMI;
//Vacuum with Fastener.
extern int Fastener_Detection_Baseline_HMI;
//Top Of Stroke Position.
extern int TOS_Position_HMI;
//Modbus Coil 34 Read.
extern int Read_Coil_34;
//Modbus Coil 34 Write.
extern int Write_Coil_34;
//Modbus Coil 38 Read.
extern int Read_Coil_38;
//Modbus Coil 38 Write.
extern int Write_Coil_38;
//Modbus Coil 40 Read.
extern int Read_Coil_40;
//Modbus Coil 40 Write.
extern int Write_Coil_40;
//Modbus Coil 48 Read.
extern int Read_Coil_48;
//Modbus Coil 48 Write.
extern int Write_Coil_48;
//Modbus Coil 49 Read.
extern bool Read_Coil_49;
//Modbus Coil 49 Write.
extern bool Write_Coil_49;
//Modbus Coil 73 Read.
extern int Read_Coil_73;
//Modbus Coil 73 Write.
extern int Write_Coil_73;
//Up Travel HMI.
extern int Up_Travel_HMI;
//TPS Set HMI.
extern bool TPS_Set_HMI;
//Error Number HMI
extern int Error_Number_HMI;
//Fastener Detection Retry HMI
extern int Fastener_Detection_Retry_HMI;
//Error screen active.
extern int Error_Screen_Active_HMI;

//MAS HMI Variables.
//Auto Tooling HMI.
extern int Tooling_Mode_HMI;
//MAS Activation.
extern bool MAS_Vibrate_Mode_HMI;
//MAS Manual feed signal.
extern bool MAS_Feed_HMI;
//Feed fastener time.
extern int MAS_Blow_Off_Time_HMI;
//MAS Eject Time. Is time that Push bar will be opened before blow off is active.
extern int MAS_Eject_Time_HMI;
//MAS Vibration Time when a fastener is send.
extern int MAS_Vibration_Time_HMI;
//MAS Vibration percentage. Use MAS_Automatic to turn on vibration continuesly or intermittent.
extern int MAS_Vibration_Percentage_HMI;
//MAS Delay time. Delay before shuttle will move to front position.
extern int MAS_Delay_Time_HMI;
//Vacuum On HMI.
extern int Vacuum_Mode_HMI;

//Global Timer Variables.
extern bool Timer1_Q;
extern bool Timer1_Start;

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#endif